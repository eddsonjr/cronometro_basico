package com.edsonjr.cronometro

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import com.edsonjr.cronometro.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding:ActivityMainBinding
    private var running: Boolean = false
    private var pause: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.iniciarBTN.setOnClickListener {
            IniciarCronometro()
        }

        binding.pausarBTN.setOnClickListener {
            PausarCronometro()
        }

        binding.zerarBTN.setOnClickListener {
            ZerarCronometro()
        }



    }


    private fun IniciarCronometro(){
        if(!running){
            /*Tempo real decorrido do cronometro*/
            binding.cronometro.base = SystemClock.elapsedRealtime() - pause
            binding.cronometro.start()
            running = true
        }
    }


    private fun PausarCronometro(){
        if(running){
            binding.cronometro.stop()
            pause = SystemClock.elapsedRealtime() - binding.cronometro.base
            running = false
        }

    }

    private fun ZerarCronometro(){
        binding.cronometro.base = SystemClock.elapsedRealtime()
        pause = 0
    }


}